#!/bin/sh
javac -cp src/main/java/*

#1
echo "big brown fox jumped over a lazy dog"
java -cp src/main/java/ WordsReverse "big brown fox jumped over a lazy dog"
echo

#2
echo "big dog!"
java -cp src/main/java/ WordsReverse "big dog!"
echo

#3
echo "big, dog"
java -cp src/main/java/ WordsReverse "big, dog"
echo

#4
echo "  big   dog"
java -cp src/main/java/ WordsReverse "  big   dog"
echo

#5
echo "(big brow dog)"
java -cp src/main/java/ WordsReverse "(big brow dog)"
echo

