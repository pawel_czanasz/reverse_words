# Reverse words #

Simple program that reverse words in given sentence.

# How to run? #

If you want to run few premade examples:
```
#!bash
./run_examples.sh
```

If you want to run your examples:
```
#!bash
./run.sh "your sentence"
```