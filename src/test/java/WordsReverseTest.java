import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class WordsReverseTest {

    private String wordReturned;

    @Test
    public void normalCaseTest() {
        wordReturned = WordsReverse.reverseWords("big brown fox jumped over a lazy dog");
        assertEquals("gib nworb xof depmuj revo a yzal god", wordReturned);
    }

    @Test
    public void moreThanOneSpaceBetweenWordsTest() {
        wordReturned = WordsReverse.reverseWords("big            dog");
        assertEquals("gib            god", wordReturned);
    }

    @Test
    public void spaceAtTheEndOfSentenceTest() {
        wordReturned = WordsReverse.reverseWords("big dog ");
        assertEquals("gib god ", wordReturned);
    }

    @Test
    public void spaceAtTheBeginningOfSentenceTest() {
        wordReturned = WordsReverse.reverseWords(" big dog");
        assertEquals(" gib god", wordReturned);
    }

    @Test
    public void multipleSpacesAtTheEndSentenceTest() {
        wordReturned = WordsReverse.reverseWords("big dog   ");
        assertEquals("gib god   ", wordReturned);
    }

    @Test
    public void oneWordTest() {
        wordReturned = WordsReverse.reverseWords("big");
        assertEquals("gib", wordReturned);
    }

    @Test
    public void noWordsTest() {
        wordReturned = WordsReverse.reverseWords(" ");
        assertEquals(" ", wordReturned);
    }

    @Test
    public void specialCharactersTest() {
        wordReturned = WordsReverse.reverseWords("big dog!");
        assertEquals("gib god!", wordReturned);

        wordReturned = WordsReverse.reverseWords("big, dog!");
        assertEquals("gib, god!", wordReturned);

        wordReturned = WordsReverse.reverseWords("big, dog!?");
        assertEquals("gib, god!?", wordReturned);

        wordReturned = WordsReverse.reverseWords("?!&$");
        assertEquals("?!&$", wordReturned);

        wordReturned = WordsReverse.reverseWords("big?!&$dog");
        assertEquals("gib?!&$god", wordReturned);
    }

}
