
public class WordsReverse {

    public static void main(String[] args) {
        if(args.length == 0)
        {
            System.out.println("Proper Usage is: java program string");
            return;
        }
        String test = args[0];
        System.out.println(reverseWords(test));
    }

    public static String reverseWords(String sentence) {
        int sentenceLength = sentence.length();
        StringBuilder result = new StringBuilder(sentenceLength);
        int tail = 0;
        for(int head = 0; head < sentenceLength; ++head) {
            if(!Character.isLetter(sentence.charAt(head))) {
                for(int i = head - 1; i >= tail; --i) {
                    result.append(sentence.charAt(i));
                }
                result.append(sentence.charAt(head));
                tail = head + 1;
            }
        }
        for(int head = sentenceLength - 1; head >= tail; --head) {
            result.append(sentence.charAt(head));
        }
        return result.toString();
    }
}
